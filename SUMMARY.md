# Summary

* [Introduction](README.md)
* [General documentation](gen_doc.md)
* [DevOps](devops.md)
* [Sysadmin](sysadmin.md)
* [Projects](projects.md)

